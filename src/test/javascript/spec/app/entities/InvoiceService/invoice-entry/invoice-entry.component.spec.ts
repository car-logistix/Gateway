/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceEntryComponent } from 'app/entities/InvoiceService/invoice-entry/invoice-entry.component';
import { InvoiceEntryService } from 'app/entities/InvoiceService/invoice-entry/invoice-entry.service';
import { InvoiceEntry } from 'app/shared/model/InvoiceService/invoice-entry.model';

describe('Component Tests', () => {
    describe('InvoiceEntry Management Component', () => {
        let comp: InvoiceEntryComponent;
        let fixture: ComponentFixture<InvoiceEntryComponent>;
        let service: InvoiceEntryService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceEntryComponent],
                providers: []
            })
                .overrideTemplate(InvoiceEntryComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(InvoiceEntryComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InvoiceEntryService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new InvoiceEntry(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.invoiceEntries[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
