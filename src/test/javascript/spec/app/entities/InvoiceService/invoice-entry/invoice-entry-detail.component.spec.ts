/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceEntryDetailComponent } from 'app/entities/InvoiceService/invoice-entry/invoice-entry-detail.component';
import { InvoiceEntry } from 'app/shared/model/InvoiceService/invoice-entry.model';

describe('Component Tests', () => {
    describe('InvoiceEntry Management Detail Component', () => {
        let comp: InvoiceEntryDetailComponent;
        let fixture: ComponentFixture<InvoiceEntryDetailComponent>;
        const route = ({ data: of({ invoiceEntry: new InvoiceEntry(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceEntryDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(InvoiceEntryDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(InvoiceEntryDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.invoiceEntry).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
