/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceEntryUpdateComponent } from 'app/entities/InvoiceService/invoice-entry/invoice-entry-update.component';
import { InvoiceEntryService } from 'app/entities/InvoiceService/invoice-entry/invoice-entry.service';
import { InvoiceEntry } from 'app/shared/model/InvoiceService/invoice-entry.model';

describe('Component Tests', () => {
    describe('InvoiceEntry Management Update Component', () => {
        let comp: InvoiceEntryUpdateComponent;
        let fixture: ComponentFixture<InvoiceEntryUpdateComponent>;
        let service: InvoiceEntryService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceEntryUpdateComponent]
            })
                .overrideTemplate(InvoiceEntryUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(InvoiceEntryUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InvoiceEntryService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new InvoiceEntry(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.invoiceEntry = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new InvoiceEntry();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.invoiceEntry = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
