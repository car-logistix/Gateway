/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceEntryDeleteDialogComponent } from 'app/entities/InvoiceService/invoice-entry/invoice-entry-delete-dialog.component';
import { InvoiceEntryService } from 'app/entities/InvoiceService/invoice-entry/invoice-entry.service';

describe('Component Tests', () => {
    describe('InvoiceEntry Management Delete Component', () => {
        let comp: InvoiceEntryDeleteDialogComponent;
        let fixture: ComponentFixture<InvoiceEntryDeleteDialogComponent>;
        let service: InvoiceEntryService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceEntryDeleteDialogComponent]
            })
                .overrideTemplate(InvoiceEntryDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(InvoiceEntryDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InvoiceEntryService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
