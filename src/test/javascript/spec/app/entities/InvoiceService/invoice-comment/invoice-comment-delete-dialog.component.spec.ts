/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceCommentDeleteDialogComponent } from 'app/entities/InvoiceService/invoice-comment/invoice-comment-delete-dialog.component';
import { InvoiceCommentService } from 'app/entities/InvoiceService/invoice-comment/invoice-comment.service';

describe('Component Tests', () => {
    describe('InvoiceComment Management Delete Component', () => {
        let comp: InvoiceCommentDeleteDialogComponent;
        let fixture: ComponentFixture<InvoiceCommentDeleteDialogComponent>;
        let service: InvoiceCommentService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceCommentDeleteDialogComponent]
            })
                .overrideTemplate(InvoiceCommentDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(InvoiceCommentDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InvoiceCommentService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
