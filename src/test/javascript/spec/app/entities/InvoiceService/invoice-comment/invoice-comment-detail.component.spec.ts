/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceCommentDetailComponent } from 'app/entities/InvoiceService/invoice-comment/invoice-comment-detail.component';
import { InvoiceComment } from 'app/shared/model/InvoiceService/invoice-comment.model';

describe('Component Tests', () => {
    describe('InvoiceComment Management Detail Component', () => {
        let comp: InvoiceCommentDetailComponent;
        let fixture: ComponentFixture<InvoiceCommentDetailComponent>;
        const route = ({ data: of({ invoiceComment: new InvoiceComment(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceCommentDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(InvoiceCommentDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(InvoiceCommentDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.invoiceComment).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
