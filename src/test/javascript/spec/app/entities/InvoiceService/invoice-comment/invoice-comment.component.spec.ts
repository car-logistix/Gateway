/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceCommentComponent } from 'app/entities/InvoiceService/invoice-comment/invoice-comment.component';
import { InvoiceCommentService } from 'app/entities/InvoiceService/invoice-comment/invoice-comment.service';
import { InvoiceComment } from 'app/shared/model/InvoiceService/invoice-comment.model';

describe('Component Tests', () => {
    describe('InvoiceComment Management Component', () => {
        let comp: InvoiceCommentComponent;
        let fixture: ComponentFixture<InvoiceCommentComponent>;
        let service: InvoiceCommentService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceCommentComponent],
                providers: []
            })
                .overrideTemplate(InvoiceCommentComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(InvoiceCommentComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InvoiceCommentService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new InvoiceComment(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.invoiceComments[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
