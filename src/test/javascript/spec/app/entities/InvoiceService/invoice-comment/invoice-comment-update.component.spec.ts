/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceCommentUpdateComponent } from 'app/entities/InvoiceService/invoice-comment/invoice-comment-update.component';
import { InvoiceCommentService } from 'app/entities/InvoiceService/invoice-comment/invoice-comment.service';
import { InvoiceComment } from 'app/shared/model/InvoiceService/invoice-comment.model';

describe('Component Tests', () => {
    describe('InvoiceComment Management Update Component', () => {
        let comp: InvoiceCommentUpdateComponent;
        let fixture: ComponentFixture<InvoiceCommentUpdateComponent>;
        let service: InvoiceCommentService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceCommentUpdateComponent]
            })
                .overrideTemplate(InvoiceCommentUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(InvoiceCommentUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InvoiceCommentService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new InvoiceComment(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.invoiceComment = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new InvoiceComment();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.invoiceComment = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
