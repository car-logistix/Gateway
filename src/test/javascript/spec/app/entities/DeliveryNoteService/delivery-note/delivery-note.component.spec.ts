/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../../test.module';
import { DeliveryNoteComponent } from 'app/entities/DeliveryNoteService/delivery-note/delivery-note.component';
import { DeliveryNoteService } from 'app/entities/DeliveryNoteService/delivery-note/delivery-note.service';
import { DeliveryNote } from 'app/shared/model/DeliveryNoteService/delivery-note.model';

describe('Component Tests', () => {
    describe('DeliveryNote Management Component', () => {
        let comp: DeliveryNoteComponent;
        let fixture: ComponentFixture<DeliveryNoteComponent>;
        let service: DeliveryNoteService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [DeliveryNoteComponent],
                providers: []
            })
                .overrideTemplate(DeliveryNoteComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DeliveryNoteComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DeliveryNoteService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new DeliveryNote(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.deliveryNotes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
