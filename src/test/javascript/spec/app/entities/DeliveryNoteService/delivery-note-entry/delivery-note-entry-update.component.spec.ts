/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { DeliveryNoteEntryUpdateComponent } from 'app/entities/DeliveryNoteService/delivery-note-entry/delivery-note-entry-update.component';
import { DeliveryNoteEntryService } from 'app/entities/DeliveryNoteService/delivery-note-entry/delivery-note-entry.service';
import { DeliveryNoteEntry } from 'app/shared/model/DeliveryNoteService/delivery-note-entry.model';

describe('Component Tests', () => {
    describe('DeliveryNoteEntry Management Update Component', () => {
        let comp: DeliveryNoteEntryUpdateComponent;
        let fixture: ComponentFixture<DeliveryNoteEntryUpdateComponent>;
        let service: DeliveryNoteEntryService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [DeliveryNoteEntryUpdateComponent]
            })
                .overrideTemplate(DeliveryNoteEntryUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DeliveryNoteEntryUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DeliveryNoteEntryService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new DeliveryNoteEntry(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.deliveryNoteEntry = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new DeliveryNoteEntry();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.deliveryNoteEntry = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
