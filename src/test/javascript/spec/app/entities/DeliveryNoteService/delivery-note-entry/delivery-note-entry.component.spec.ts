/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../../test.module';
import { DeliveryNoteEntryComponent } from 'app/entities/DeliveryNoteService/delivery-note-entry/delivery-note-entry.component';
import { DeliveryNoteEntryService } from 'app/entities/DeliveryNoteService/delivery-note-entry/delivery-note-entry.service';
import { DeliveryNoteEntry } from 'app/shared/model/DeliveryNoteService/delivery-note-entry.model';

describe('Component Tests', () => {
    describe('DeliveryNoteEntry Management Component', () => {
        let comp: DeliveryNoteEntryComponent;
        let fixture: ComponentFixture<DeliveryNoteEntryComponent>;
        let service: DeliveryNoteEntryService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [DeliveryNoteEntryComponent],
                providers: []
            })
                .overrideTemplate(DeliveryNoteEntryComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DeliveryNoteEntryComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DeliveryNoteEntryService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new DeliveryNoteEntry(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.deliveryNoteEntries[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
