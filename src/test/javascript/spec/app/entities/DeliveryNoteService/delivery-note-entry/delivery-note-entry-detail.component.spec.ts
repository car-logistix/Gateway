/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { DeliveryNoteEntryDetailComponent } from 'app/entities/DeliveryNoteService/delivery-note-entry/delivery-note-entry-detail.component';
import { DeliveryNoteEntry } from 'app/shared/model/DeliveryNoteService/delivery-note-entry.model';

describe('Component Tests', () => {
    describe('DeliveryNoteEntry Management Detail Component', () => {
        let comp: DeliveryNoteEntryDetailComponent;
        let fixture: ComponentFixture<DeliveryNoteEntryDetailComponent>;
        const route = ({ data: of({ deliveryNoteEntry: new DeliveryNoteEntry(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [DeliveryNoteEntryDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(DeliveryNoteEntryDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DeliveryNoteEntryDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.deliveryNoteEntry).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
