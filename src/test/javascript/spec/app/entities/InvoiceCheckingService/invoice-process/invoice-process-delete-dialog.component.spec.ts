/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceProcessDeleteDialogComponent } from 'app/entities/InvoiceCheckingService/invoice-process/invoice-process-delete-dialog.component';
import { InvoiceProcessService } from 'app/entities/InvoiceCheckingService/invoice-process/invoice-process.service';

describe('Component Tests', () => {
    describe('InvoiceProcess Management Delete Component', () => {
        let comp: InvoiceProcessDeleteDialogComponent;
        let fixture: ComponentFixture<InvoiceProcessDeleteDialogComponent>;
        let service: InvoiceProcessService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceProcessDeleteDialogComponent]
            })
                .overrideTemplate(InvoiceProcessDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(InvoiceProcessDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InvoiceProcessService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
