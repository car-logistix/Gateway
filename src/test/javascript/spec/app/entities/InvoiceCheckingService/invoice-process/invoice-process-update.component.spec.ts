/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceProcessUpdateComponent } from 'app/entities/InvoiceCheckingService/invoice-process/invoice-process-update.component';
import { InvoiceProcessService } from 'app/entities/InvoiceCheckingService/invoice-process/invoice-process.service';
import { InvoiceProcess } from 'app/shared/model/InvoiceCheckingService/invoice-process.model';

describe('Component Tests', () => {
    describe('InvoiceProcess Management Update Component', () => {
        let comp: InvoiceProcessUpdateComponent;
        let fixture: ComponentFixture<InvoiceProcessUpdateComponent>;
        let service: InvoiceProcessService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceProcessUpdateComponent]
            })
                .overrideTemplate(InvoiceProcessUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(InvoiceProcessUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InvoiceProcessService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new InvoiceProcess(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.invoiceProcess = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new InvoiceProcess();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.invoiceProcess = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
