/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceProcessDetailComponent } from 'app/entities/InvoiceCheckingService/invoice-process/invoice-process-detail.component';
import { InvoiceProcess } from 'app/shared/model/InvoiceCheckingService/invoice-process.model';

describe('Component Tests', () => {
    describe('InvoiceProcess Management Detail Component', () => {
        let comp: InvoiceProcessDetailComponent;
        let fixture: ComponentFixture<InvoiceProcessDetailComponent>;
        const route = ({ data: of({ invoiceProcess: new InvoiceProcess(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceProcessDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(InvoiceProcessDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(InvoiceProcessDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.invoiceProcess).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
