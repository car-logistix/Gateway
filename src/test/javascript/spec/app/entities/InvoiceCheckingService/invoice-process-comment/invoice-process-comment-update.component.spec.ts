/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceProcessCommentUpdateComponent } from 'app/entities/InvoiceCheckingService/invoice-process-comment/invoice-process-comment-update.component';
import { InvoiceProcessCommentService } from 'app/entities/InvoiceCheckingService/invoice-process-comment/invoice-process-comment.service';
import { InvoiceProcessComment } from 'app/shared/model/InvoiceCheckingService/invoice-process-comment.model';

describe('Component Tests', () => {
    describe('InvoiceProcessComment Management Update Component', () => {
        let comp: InvoiceProcessCommentUpdateComponent;
        let fixture: ComponentFixture<InvoiceProcessCommentUpdateComponent>;
        let service: InvoiceProcessCommentService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceProcessCommentUpdateComponent]
            })
                .overrideTemplate(InvoiceProcessCommentUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(InvoiceProcessCommentUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InvoiceProcessCommentService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new InvoiceProcessComment(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.invoiceProcessComment = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new InvoiceProcessComment();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.invoiceProcessComment = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
