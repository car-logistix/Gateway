/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceProcessCommentComponent } from 'app/entities/InvoiceCheckingService/invoice-process-comment/invoice-process-comment.component';
import { InvoiceProcessCommentService } from 'app/entities/InvoiceCheckingService/invoice-process-comment/invoice-process-comment.service';
import { InvoiceProcessComment } from 'app/shared/model/InvoiceCheckingService/invoice-process-comment.model';

describe('Component Tests', () => {
    describe('InvoiceProcessComment Management Component', () => {
        let comp: InvoiceProcessCommentComponent;
        let fixture: ComponentFixture<InvoiceProcessCommentComponent>;
        let service: InvoiceProcessCommentService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceProcessCommentComponent],
                providers: []
            })
                .overrideTemplate(InvoiceProcessCommentComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(InvoiceProcessCommentComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InvoiceProcessCommentService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new InvoiceProcessComment(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.invoiceProcessComments[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
