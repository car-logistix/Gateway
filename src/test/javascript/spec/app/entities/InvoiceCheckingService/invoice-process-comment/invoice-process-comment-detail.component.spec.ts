/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceProcessCommentDetailComponent } from 'app/entities/InvoiceCheckingService/invoice-process-comment/invoice-process-comment-detail.component';
import { InvoiceProcessComment } from 'app/shared/model/InvoiceCheckingService/invoice-process-comment.model';

describe('Component Tests', () => {
    describe('InvoiceProcessComment Management Detail Component', () => {
        let comp: InvoiceProcessCommentDetailComponent;
        let fixture: ComponentFixture<InvoiceProcessCommentDetailComponent>;
        const route = ({ data: of({ invoiceProcessComment: new InvoiceProcessComment(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceProcessCommentDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(InvoiceProcessCommentDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(InvoiceProcessCommentDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.invoiceProcessComment).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
