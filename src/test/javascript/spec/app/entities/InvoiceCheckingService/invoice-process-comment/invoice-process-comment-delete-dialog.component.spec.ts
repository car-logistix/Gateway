/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { GatewayTestModule } from '../../../../test.module';
import { InvoiceProcessCommentDeleteDialogComponent } from 'app/entities/InvoiceCheckingService/invoice-process-comment/invoice-process-comment-delete-dialog.component';
import { InvoiceProcessCommentService } from 'app/entities/InvoiceCheckingService/invoice-process-comment/invoice-process-comment.service';

describe('Component Tests', () => {
    describe('InvoiceProcessComment Management Delete Component', () => {
        let comp: InvoiceProcessCommentDeleteDialogComponent;
        let fixture: ComponentFixture<InvoiceProcessCommentDeleteDialogComponent>;
        let service: InvoiceProcessCommentService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [InvoiceProcessCommentDeleteDialogComponent]
            })
                .overrideTemplate(InvoiceProcessCommentDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(InvoiceProcessCommentDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InvoiceProcessCommentService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
