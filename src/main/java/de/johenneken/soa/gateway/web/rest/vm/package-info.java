/**
 * View Models used by Spring MVC REST controllers.
 */
package de.johenneken.soa.gateway.web.rest.vm;
