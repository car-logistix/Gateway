import { Moment } from 'moment';
import { IInvoiceProcessComment } from 'app/shared/model/InvoiceCheckingService/invoice-process-comment.model';

export const enum InvoiceProcessStatus {
    INCOMING = 'INCOMING',
    FINISHED = 'FINISHED',
    ESCALATED = 'ESCALATED'
}

export interface IInvoiceProcess {
    id?: number;
    status?: InvoiceProcessStatus;
    invoiceId?: number;
    deliveryNoteId?: number;
    postedBy?: string;
    postedAt?: Moment;
    comments?: IInvoiceProcessComment[];
}

export class InvoiceProcess implements IInvoiceProcess {
    constructor(
        public id?: number,
        public status?: InvoiceProcessStatus,
        public invoiceId?: number,
        public deliveryNoteId?: number,
        public postedBy?: string,
        public postedAt?: Moment,
        public comments?: IInvoiceProcessComment[]
    ) {}
}
