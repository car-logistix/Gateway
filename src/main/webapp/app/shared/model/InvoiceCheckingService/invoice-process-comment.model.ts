import { IInvoiceProcess } from 'app/shared/model/InvoiceCheckingService/invoice-process.model';

export interface IInvoiceProcessComment {
    id?: number;
    text?: string;
    user?: string;
    invoiceProcess?: IInvoiceProcess;
}

export class InvoiceProcessComment implements IInvoiceProcessComment {
    constructor(public id?: number, public text?: string, public user?: string, public invoiceProcess?: IInvoiceProcess) {}
}
