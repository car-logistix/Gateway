import { IInvoice } from 'app/shared/model/InvoiceService/invoice.model';

export interface IInvoiceEntry {
    id?: number;
    description?: string;
    amount?: number;
    price?: number;
    invoice?: IInvoice;
}

export class InvoiceEntry implements IInvoiceEntry {
    constructor(
        public id?: number,
        public description?: string,
        public amount?: number,
        public price?: number,
        public invoice?: IInvoice
    ) {}
}
