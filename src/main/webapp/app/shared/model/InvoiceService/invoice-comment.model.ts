import { IInvoice } from 'app/shared/model/InvoiceService/invoice.model';

export interface IInvoiceComment {
    id?: number;
    text?: string;
    user?: string;
    invoice?: IInvoice;
}

export class InvoiceComment implements IInvoiceComment {
    constructor(public id?: number, public text?: string, public user?: string, public invoice?: IInvoice) {}
}
