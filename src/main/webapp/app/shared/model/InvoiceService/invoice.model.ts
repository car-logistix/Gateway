import { Moment } from 'moment';
import { IInvoiceComment } from 'app/shared/model/InvoiceService/invoice-comment.model';
import { IInvoiceEntry } from 'app/shared/model/InvoiceService/invoice-entry.model';

export const enum InvoiceStatus {
    NEW = 'NEW',
    SENT = 'SENT',
    ACCEPTED = 'ACCEPTED',
    FINISHED = 'FINISHED'
}

export interface IInvoice {
    id?: number;
    invoiceNummer?: string;
    dateIssued?: Moment;
    invoiceAdress?: string;
    issuerAdress?: string;
    issuerFirm?: string;
    dueDate?: Moment;
    status?: InvoiceStatus;
    receiver?: string;
    total?: number;
    comments?: IInvoiceComment[];
    entries?: IInvoiceEntry[];
}

export class Invoice implements IInvoice {
    constructor(
        public id?: number,
        public invoiceNummer?: string,
        public dateIssued?: Moment,
        public invoiceAdress?: string,
        public issuerAdress?: string,
        public issuerFirm?: string,
        public dueDate?: Moment,
        public status?: InvoiceStatus,
        public receiver?: string,
        public total?: number,
        public comments?: IInvoiceComment[],
        public entries?: IInvoiceEntry[]
    ) {}
}
