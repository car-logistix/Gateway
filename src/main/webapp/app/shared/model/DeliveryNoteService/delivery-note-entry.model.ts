import { IDeliveryNote } from 'app/shared/model/DeliveryNoteService/delivery-note.model';

export interface IDeliveryNoteEntry {
    id?: number;
    description?: string;
    amount?: number;
    deliveryNote?: IDeliveryNote;
}

export class DeliveryNoteEntry implements IDeliveryNoteEntry {
    constructor(public id?: number, public description?: string, public amount?: number, public deliveryNote?: IDeliveryNote) {}
}
