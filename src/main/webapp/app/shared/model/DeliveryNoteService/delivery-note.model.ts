import { Moment } from 'moment';
import { IDeliveryNoteEntry } from 'app/shared/model/DeliveryNoteService/delivery-note-entry.model';

export interface IDeliveryNote {
    id?: number;
    noteNumber?: string;
    deliveryAdress?: string;
    supplierFirm?: string;
    supplierAdress?: string;
    issueDate?: Moment;
    finished?: boolean;
    principal?: string;
    entries?: IDeliveryNoteEntry[];
}

export class DeliveryNote implements IDeliveryNote {
    constructor(
        public id?: number,
        public noteNumber?: string,
        public deliveryAdress?: string,
        public supplierFirm?: string,
        public supplierAdress?: string,
        public issueDate?: Moment,
        public finished?: boolean,
        public principal?: string,
        public entries?: IDeliveryNoteEntry[]
    ) {
        this.finished = this.finished || false;
    }
}
