import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IInvoiceEntry } from 'app/shared/model/InvoiceService/invoice-entry.model';
import { InvoiceEntryService } from './invoice-entry.service';

@Component({
    selector: 'jhi-invoice-entry-delete-dialog',
    templateUrl: './invoice-entry-delete-dialog.component.html'
})
export class InvoiceEntryDeleteDialogComponent {
    invoiceEntry: IInvoiceEntry;

    constructor(
        private invoiceEntryService: InvoiceEntryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.invoiceEntryService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'invoiceEntryListModification',
                content: 'Deleted an invoiceEntry'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-invoice-entry-delete-popup',
    template: ''
})
export class InvoiceEntryDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ invoiceEntry }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(InvoiceEntryDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.invoiceEntry = invoiceEntry;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
