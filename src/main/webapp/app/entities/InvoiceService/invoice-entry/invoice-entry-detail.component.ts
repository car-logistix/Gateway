import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IInvoiceEntry } from 'app/shared/model/InvoiceService/invoice-entry.model';

@Component({
    selector: 'jhi-invoice-entry-detail',
    templateUrl: './invoice-entry-detail.component.html'
})
export class InvoiceEntryDetailComponent implements OnInit {
    invoiceEntry: IInvoiceEntry;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ invoiceEntry }) => {
            this.invoiceEntry = invoiceEntry;
        });
    }

    previousState() {
        window.history.back();
    }
}
