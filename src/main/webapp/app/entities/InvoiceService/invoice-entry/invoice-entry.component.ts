import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IInvoiceEntry } from 'app/shared/model/InvoiceService/invoice-entry.model';
import { Principal } from 'app/core';
import { InvoiceEntryService } from './invoice-entry.service';

@Component({
    selector: 'jhi-invoice-entry',
    templateUrl: './invoice-entry.component.html'
})
export class InvoiceEntryComponent implements OnInit, OnDestroy {
    invoiceEntries: IInvoiceEntry[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private invoiceEntryService: InvoiceEntryService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.invoiceEntryService.query().subscribe(
            (res: HttpResponse<IInvoiceEntry[]>) => {
                this.invoiceEntries = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInInvoiceEntries();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IInvoiceEntry) {
        return item.id;
    }

    registerChangeInInvoiceEntries() {
        this.eventSubscriber = this.eventManager.subscribe('invoiceEntryListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
