import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { InvoiceEntry } from 'app/shared/model/InvoiceService/invoice-entry.model';
import { InvoiceEntryService } from './invoice-entry.service';
import { InvoiceEntryComponent } from './invoice-entry.component';
import { InvoiceEntryDetailComponent } from './invoice-entry-detail.component';
import { InvoiceEntryUpdateComponent } from './invoice-entry-update.component';
import { InvoiceEntryDeletePopupComponent } from './invoice-entry-delete-dialog.component';
import { IInvoiceEntry } from 'app/shared/model/InvoiceService/invoice-entry.model';

@Injectable({ providedIn: 'root' })
export class InvoiceEntryResolve implements Resolve<IInvoiceEntry> {
    constructor(private service: InvoiceEntryService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((invoiceEntry: HttpResponse<InvoiceEntry>) => invoiceEntry.body));
        }
        return of(new InvoiceEntry());
    }
}

export const invoiceEntryRoute: Routes = [
    {
        path: 'invoice-entry',
        component: InvoiceEntryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceEntries'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice-entry/:id/view',
        component: InvoiceEntryDetailComponent,
        resolve: {
            invoiceEntry: InvoiceEntryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceEntries'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice-entry/new',
        component: InvoiceEntryUpdateComponent,
        resolve: {
            invoiceEntry: InvoiceEntryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceEntries'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice-entry/:id/edit',
        component: InvoiceEntryUpdateComponent,
        resolve: {
            invoiceEntry: InvoiceEntryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceEntries'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const invoiceEntryPopupRoute: Routes = [
    {
        path: 'invoice-entry/:id/delete',
        component: InvoiceEntryDeletePopupComponent,
        resolve: {
            invoiceEntry: InvoiceEntryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceEntries'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
