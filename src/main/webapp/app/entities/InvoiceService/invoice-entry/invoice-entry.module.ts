import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import {
    InvoiceEntryComponent,
    InvoiceEntryDetailComponent,
    InvoiceEntryUpdateComponent,
    InvoiceEntryDeletePopupComponent,
    InvoiceEntryDeleteDialogComponent,
    invoiceEntryRoute,
    invoiceEntryPopupRoute
} from './';

const ENTITY_STATES = [...invoiceEntryRoute, ...invoiceEntryPopupRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        InvoiceEntryComponent,
        InvoiceEntryDetailComponent,
        InvoiceEntryUpdateComponent,
        InvoiceEntryDeleteDialogComponent,
        InvoiceEntryDeletePopupComponent
    ],
    entryComponents: [
        InvoiceEntryComponent,
        InvoiceEntryUpdateComponent,
        InvoiceEntryDeleteDialogComponent,
        InvoiceEntryDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayInvoiceEntryModule {}
