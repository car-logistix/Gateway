import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IInvoiceEntry } from 'app/shared/model/InvoiceService/invoice-entry.model';
import { InvoiceEntryService } from './invoice-entry.service';
import { IInvoice } from 'app/shared/model/InvoiceService/invoice.model';
import { InvoiceService } from 'app/entities/InvoiceService/invoice';

@Component({
    selector: 'jhi-invoice-entry-update',
    templateUrl: './invoice-entry-update.component.html'
})
export class InvoiceEntryUpdateComponent implements OnInit {
    private _invoiceEntry: IInvoiceEntry;
    isSaving: boolean;

    invoices: IInvoice[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private invoiceEntryService: InvoiceEntryService,
        private invoiceService: InvoiceService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ invoiceEntry }) => {
            this.invoiceEntry = invoiceEntry;
        });
        this.invoiceService.query().subscribe(
            (res: HttpResponse<IInvoice[]>) => {
                this.invoices = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.invoiceEntry.id !== undefined) {
            this.subscribeToSaveResponse(this.invoiceEntryService.update(this.invoiceEntry));
        } else {
            this.subscribeToSaveResponse(this.invoiceEntryService.create(this.invoiceEntry));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IInvoiceEntry>>) {
        result.subscribe((res: HttpResponse<IInvoiceEntry>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackInvoiceById(index: number, item: IInvoice) {
        return item.id;
    }
    get invoiceEntry() {
        return this._invoiceEntry;
    }

    set invoiceEntry(invoiceEntry: IInvoiceEntry) {
        this._invoiceEntry = invoiceEntry;
    }
}
