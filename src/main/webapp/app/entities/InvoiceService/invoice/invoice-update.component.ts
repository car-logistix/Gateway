import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IInvoice, InvoiceStatus } from 'app/shared/model/InvoiceService/invoice.model';
import { InvoiceService } from './invoice.service';
import { IInvoiceComment } from 'app/shared/model/InvoiceService/invoice-comment.model';
import { Principal } from 'app/core';

@Component({
    selector: 'jhi-invoice-update',
    templateUrl: './invoice-update.component.html'
})
export class InvoiceUpdateComponent implements OnInit {
    private _invoice: IInvoice;
    isSaving: boolean;
    dateIssuedDp: any;
    dueDateDp: any;
    account: any;
    newComment: IInvoiceComment = {};

    constructor(private invoiceService: InvoiceService, private activatedRoute: ActivatedRoute, private principal: Principal) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ invoice }) => {
            this.invoice = invoice;
            this.invoice.status = this.invoice.status || InvoiceStatus.NEW;
        });
        this.principal.identity().then(account => {
            this.account = account;
        });
    }

    previousState() {
        window.history.back();
    }

    addRow() {
        if (this.invoice.entries === undefined) {
            this.invoice.entries = [];
        }
        this.invoice.entries.push({ amount: 1 });
    }

    removeRow() {
        this.invoice.entries.pop();
    }

    addComment() {
        this.newComment.user = this.account.login;
        this.invoice.comments = this.invoice.comments || [];
        this.invoice.comments.push(this.newComment);
        this.newComment = {};
    }

    save() {
        this.invoice.receiver = this.account.login;
        this.isSaving = true;
        if (this.invoice.id !== undefined) {
            this.subscribeToSaveResponse(this.invoiceService.update(this.invoice));
        } else {
            this.subscribeToSaveResponse(this.invoiceService.create(this.invoice));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IInvoice>>) {
        result.subscribe((res: HttpResponse<IInvoice>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    get invoice() {
        return this._invoice;
    }

    set invoice(invoice: IInvoice) {
        this._invoice = invoice;
    }
}
