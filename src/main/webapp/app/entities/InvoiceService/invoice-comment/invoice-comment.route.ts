import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { InvoiceComment } from 'app/shared/model/InvoiceService/invoice-comment.model';
import { InvoiceCommentService } from './invoice-comment.service';
import { InvoiceCommentComponent } from './invoice-comment.component';
import { InvoiceCommentDetailComponent } from './invoice-comment-detail.component';
import { InvoiceCommentUpdateComponent } from './invoice-comment-update.component';
import { InvoiceCommentDeletePopupComponent } from './invoice-comment-delete-dialog.component';
import { IInvoiceComment } from 'app/shared/model/InvoiceService/invoice-comment.model';

@Injectable({ providedIn: 'root' })
export class InvoiceCommentResolve implements Resolve<IInvoiceComment> {
    constructor(private service: InvoiceCommentService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((invoiceComment: HttpResponse<InvoiceComment>) => invoiceComment.body));
        }
        return of(new InvoiceComment());
    }
}

export const invoiceCommentRoute: Routes = [
    {
        path: 'invoice-comment',
        component: InvoiceCommentComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceComments'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice-comment/:id/view',
        component: InvoiceCommentDetailComponent,
        resolve: {
            invoiceComment: InvoiceCommentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceComments'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice-comment/new',
        component: InvoiceCommentUpdateComponent,
        resolve: {
            invoiceComment: InvoiceCommentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceComments'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice-comment/:id/edit',
        component: InvoiceCommentUpdateComponent,
        resolve: {
            invoiceComment: InvoiceCommentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceComments'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const invoiceCommentPopupRoute: Routes = [
    {
        path: 'invoice-comment/:id/delete',
        component: InvoiceCommentDeletePopupComponent,
        resolve: {
            invoiceComment: InvoiceCommentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceComments'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
