import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IInvoiceComment } from 'app/shared/model/InvoiceService/invoice-comment.model';

type EntityResponseType = HttpResponse<IInvoiceComment>;
type EntityArrayResponseType = HttpResponse<IInvoiceComment[]>;

@Injectable({ providedIn: 'root' })
export class InvoiceCommentService {
    private resourceUrl = SERVER_API_URL + 'invoiceservice/api/invoice-comments';

    constructor(private http: HttpClient) {}

    create(invoiceComment: IInvoiceComment): Observable<EntityResponseType> {
        return this.http.post<IInvoiceComment>(this.resourceUrl, invoiceComment, { observe: 'response' });
    }

    update(invoiceComment: IInvoiceComment): Observable<EntityResponseType> {
        return this.http.put<IInvoiceComment>(this.resourceUrl, invoiceComment, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IInvoiceComment>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IInvoiceComment[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
