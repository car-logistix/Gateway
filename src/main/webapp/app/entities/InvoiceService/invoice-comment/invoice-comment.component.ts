import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IInvoiceComment } from 'app/shared/model/InvoiceService/invoice-comment.model';
import { Principal } from 'app/core';
import { InvoiceCommentService } from './invoice-comment.service';

@Component({
    selector: 'jhi-invoice-comment',
    templateUrl: './invoice-comment.component.html'
})
export class InvoiceCommentComponent implements OnInit, OnDestroy {
    invoiceComments: IInvoiceComment[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private invoiceCommentService: InvoiceCommentService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.invoiceCommentService.query().subscribe(
            (res: HttpResponse<IInvoiceComment[]>) => {
                this.invoiceComments = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInInvoiceComments();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IInvoiceComment) {
        return item.id;
    }

    registerChangeInInvoiceComments() {
        this.eventSubscriber = this.eventManager.subscribe('invoiceCommentListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
