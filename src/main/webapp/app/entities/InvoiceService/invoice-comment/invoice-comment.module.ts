import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import {
    InvoiceCommentComponent,
    InvoiceCommentDetailComponent,
    InvoiceCommentUpdateComponent,
    InvoiceCommentDeletePopupComponent,
    InvoiceCommentDeleteDialogComponent,
    invoiceCommentRoute,
    invoiceCommentPopupRoute
} from './';

const ENTITY_STATES = [...invoiceCommentRoute, ...invoiceCommentPopupRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        InvoiceCommentComponent,
        InvoiceCommentDetailComponent,
        InvoiceCommentUpdateComponent,
        InvoiceCommentDeleteDialogComponent,
        InvoiceCommentDeletePopupComponent
    ],
    entryComponents: [
        InvoiceCommentComponent,
        InvoiceCommentUpdateComponent,
        InvoiceCommentDeleteDialogComponent,
        InvoiceCommentDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayInvoiceCommentModule {}
