import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IInvoiceComment } from 'app/shared/model/InvoiceService/invoice-comment.model';
import { InvoiceCommentService } from './invoice-comment.service';
import { IInvoice } from 'app/shared/model/InvoiceService/invoice.model';
import { InvoiceService } from 'app/entities/InvoiceService/invoice';

@Component({
    selector: 'jhi-invoice-comment-update',
    templateUrl: './invoice-comment-update.component.html'
})
export class InvoiceCommentUpdateComponent implements OnInit {
    private _invoiceComment: IInvoiceComment;
    isSaving: boolean;

    invoices: IInvoice[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private invoiceCommentService: InvoiceCommentService,
        private invoiceService: InvoiceService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ invoiceComment }) => {
            this.invoiceComment = invoiceComment;
        });
        this.invoiceService.query().subscribe(
            (res: HttpResponse<IInvoice[]>) => {
                this.invoices = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.invoiceComment.id !== undefined) {
            this.subscribeToSaveResponse(this.invoiceCommentService.update(this.invoiceComment));
        } else {
            this.subscribeToSaveResponse(this.invoiceCommentService.create(this.invoiceComment));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IInvoiceComment>>) {
        result.subscribe((res: HttpResponse<IInvoiceComment>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackInvoiceById(index: number, item: IInvoice) {
        return item.id;
    }
    get invoiceComment() {
        return this._invoiceComment;
    }

    set invoiceComment(invoiceComment: IInvoiceComment) {
        this._invoiceComment = invoiceComment;
    }
}
