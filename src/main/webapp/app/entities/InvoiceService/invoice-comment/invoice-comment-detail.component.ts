import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IInvoiceComment } from 'app/shared/model/InvoiceService/invoice-comment.model';

@Component({
    selector: 'jhi-invoice-comment-detail',
    templateUrl: './invoice-comment-detail.component.html'
})
export class InvoiceCommentDetailComponent implements OnInit {
    invoiceComment: IInvoiceComment;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ invoiceComment }) => {
            this.invoiceComment = invoiceComment;
        });
    }

    previousState() {
        window.history.back();
    }
}
