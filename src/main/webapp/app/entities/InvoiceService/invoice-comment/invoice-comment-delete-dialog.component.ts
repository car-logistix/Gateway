import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IInvoiceComment } from 'app/shared/model/InvoiceService/invoice-comment.model';
import { InvoiceCommentService } from './invoice-comment.service';

@Component({
    selector: 'jhi-invoice-comment-delete-dialog',
    templateUrl: './invoice-comment-delete-dialog.component.html'
})
export class InvoiceCommentDeleteDialogComponent {
    invoiceComment: IInvoiceComment;

    constructor(
        private invoiceCommentService: InvoiceCommentService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.invoiceCommentService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'invoiceCommentListModification',
                content: 'Deleted an invoiceComment'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-invoice-comment-delete-popup',
    template: ''
})
export class InvoiceCommentDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ invoiceComment }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(InvoiceCommentDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.invoiceComment = invoiceComment;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
