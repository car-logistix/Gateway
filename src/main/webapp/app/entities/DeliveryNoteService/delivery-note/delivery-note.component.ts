import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IDeliveryNote } from 'app/shared/model/DeliveryNoteService/delivery-note.model';
import { Principal } from 'app/core';
import { DeliveryNoteService } from './delivery-note.service';

@Component({
    selector: 'jhi-delivery-note',
    templateUrl: './delivery-note.component.html'
})
export class DeliveryNoteComponent implements OnInit, OnDestroy {
    deliveryNotes: IDeliveryNote[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private deliveryNoteService: DeliveryNoteService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.deliveryNoteService.query().subscribe(
            (res: HttpResponse<IDeliveryNote[]>) => {
                this.deliveryNotes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInDeliveryNotes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IDeliveryNote) {
        return item.id;
    }

    registerChangeInDeliveryNotes() {
        this.eventSubscriber = this.eventManager.subscribe('deliveryNoteListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
