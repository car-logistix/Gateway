import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IDeliveryNote } from 'app/shared/model/DeliveryNoteService/delivery-note.model';
import { DeliveryNoteService } from './delivery-note.service';

@Component({
    selector: 'jhi-delivery-note-update',
    templateUrl: './delivery-note-update.component.html',
    styleUrls: ['delivery-note.scss']
})
export class DeliveryNoteUpdateComponent implements OnInit {
    private _deliveryNote: IDeliveryNote;
    isSaving: boolean;
    issueDateDp: any;

    constructor(private deliveryNoteService: DeliveryNoteService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ deliveryNote }) => {
            this.deliveryNote = deliveryNote;
        });
    }

    previousState() {
        window.history.back();
    }

    addRow() {
        if (this.deliveryNote.entries === undefined) {
            this.deliveryNote.entries = [];
        }
        this.deliveryNote.entries.push({ amount: 1 });
    }

    removeRow() {
        this.deliveryNote.entries.pop();
    }

    save() {
        this.isSaving = true;
        if (this.deliveryNote.id !== undefined) {
            this.subscribeToSaveResponse(this.deliveryNoteService.update(this.deliveryNote));
        } else {
            this.subscribeToSaveResponse(this.deliveryNoteService.create(this.deliveryNote));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IDeliveryNote>>) {
        result.subscribe((res: HttpResponse<IDeliveryNote>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get deliveryNote() {
        return this._deliveryNote;
    }

    set deliveryNote(deliveryNote: IDeliveryNote) {
        this._deliveryNote = deliveryNote;
    }
}
