import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DeliveryNoteEntry } from 'app/shared/model/DeliveryNoteService/delivery-note-entry.model';
import { DeliveryNoteEntryService } from './delivery-note-entry.service';
import { DeliveryNoteEntryComponent } from './delivery-note-entry.component';
import { DeliveryNoteEntryDetailComponent } from './delivery-note-entry-detail.component';
import { DeliveryNoteEntryUpdateComponent } from './delivery-note-entry-update.component';
import { DeliveryNoteEntryDeletePopupComponent } from './delivery-note-entry-delete-dialog.component';
import { IDeliveryNoteEntry } from 'app/shared/model/DeliveryNoteService/delivery-note-entry.model';

@Injectable({ providedIn: 'root' })
export class DeliveryNoteEntryResolve implements Resolve<IDeliveryNoteEntry> {
    constructor(private service: DeliveryNoteEntryService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((deliveryNoteEntry: HttpResponse<DeliveryNoteEntry>) => deliveryNoteEntry.body));
        }
        return of(new DeliveryNoteEntry());
    }
}

export const deliveryNoteEntryRoute: Routes = [
    {
        path: 'delivery-note-entry',
        component: DeliveryNoteEntryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DeliveryNoteEntries'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'delivery-note-entry/:id/view',
        component: DeliveryNoteEntryDetailComponent,
        resolve: {
            deliveryNoteEntry: DeliveryNoteEntryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DeliveryNoteEntries'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'delivery-note-entry/new',
        component: DeliveryNoteEntryUpdateComponent,
        resolve: {
            deliveryNoteEntry: DeliveryNoteEntryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DeliveryNoteEntries'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'delivery-note-entry/:id/edit',
        component: DeliveryNoteEntryUpdateComponent,
        resolve: {
            deliveryNoteEntry: DeliveryNoteEntryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DeliveryNoteEntries'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const deliveryNoteEntryPopupRoute: Routes = [
    {
        path: 'delivery-note-entry/:id/delete',
        component: DeliveryNoteEntryDeletePopupComponent,
        resolve: {
            deliveryNoteEntry: DeliveryNoteEntryResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DeliveryNoteEntries'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
