import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDeliveryNoteEntry } from 'app/shared/model/DeliveryNoteService/delivery-note-entry.model';

@Component({
    selector: 'jhi-delivery-note-entry-detail',
    templateUrl: './delivery-note-entry-detail.component.html'
})
export class DeliveryNoteEntryDetailComponent implements OnInit {
    deliveryNoteEntry: IDeliveryNoteEntry;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ deliveryNoteEntry }) => {
            this.deliveryNoteEntry = deliveryNoteEntry;
        });
    }

    previousState() {
        window.history.back();
    }
}
