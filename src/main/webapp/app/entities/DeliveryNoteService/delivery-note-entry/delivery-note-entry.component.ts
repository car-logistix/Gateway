import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IDeliveryNoteEntry } from 'app/shared/model/DeliveryNoteService/delivery-note-entry.model';
import { Principal } from 'app/core';
import { DeliveryNoteEntryService } from './delivery-note-entry.service';

@Component({
    selector: 'jhi-delivery-note-entry',
    templateUrl: './delivery-note-entry.component.html'
})
export class DeliveryNoteEntryComponent implements OnInit, OnDestroy {
    deliveryNoteEntries: IDeliveryNoteEntry[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private deliveryNoteEntryService: DeliveryNoteEntryService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.deliveryNoteEntryService.query().subscribe(
            (res: HttpResponse<IDeliveryNoteEntry[]>) => {
                this.deliveryNoteEntries = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInDeliveryNoteEntries();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IDeliveryNoteEntry) {
        return item.id;
    }

    registerChangeInDeliveryNoteEntries() {
        this.eventSubscriber = this.eventManager.subscribe('deliveryNoteEntryListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
