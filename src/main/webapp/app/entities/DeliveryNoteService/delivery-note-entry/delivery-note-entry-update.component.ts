import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IDeliveryNoteEntry } from 'app/shared/model/DeliveryNoteService/delivery-note-entry.model';
import { DeliveryNoteEntryService } from './delivery-note-entry.service';
import { IDeliveryNote } from 'app/shared/model/DeliveryNoteService/delivery-note.model';
import { DeliveryNoteService } from 'app/entities/DeliveryNoteService/delivery-note';

@Component({
    selector: 'jhi-delivery-note-entry-update',
    templateUrl: './delivery-note-entry-update.component.html'
})
export class DeliveryNoteEntryUpdateComponent implements OnInit {
    private _deliveryNoteEntry: IDeliveryNoteEntry;
    isSaving: boolean;

    deliverynotes: IDeliveryNote[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private deliveryNoteEntryService: DeliveryNoteEntryService,
        private deliveryNoteService: DeliveryNoteService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ deliveryNoteEntry }) => {
            this.deliveryNoteEntry = deliveryNoteEntry;
        });
        this.deliveryNoteService.query().subscribe(
            (res: HttpResponse<IDeliveryNote[]>) => {
                this.deliverynotes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.deliveryNoteEntry.id !== undefined) {
            this.subscribeToSaveResponse(this.deliveryNoteEntryService.update(this.deliveryNoteEntry));
        } else {
            this.subscribeToSaveResponse(this.deliveryNoteEntryService.create(this.deliveryNoteEntry));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IDeliveryNoteEntry>>) {
        result.subscribe((res: HttpResponse<IDeliveryNoteEntry>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackDeliveryNoteById(index: number, item: IDeliveryNote) {
        return item.id;
    }
    get deliveryNoteEntry() {
        return this._deliveryNoteEntry;
    }

    set deliveryNoteEntry(deliveryNoteEntry: IDeliveryNoteEntry) {
        this._deliveryNoteEntry = deliveryNoteEntry;
    }
}
