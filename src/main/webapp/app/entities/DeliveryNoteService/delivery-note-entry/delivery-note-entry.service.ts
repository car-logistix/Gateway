import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDeliveryNoteEntry } from 'app/shared/model/DeliveryNoteService/delivery-note-entry.model';

type EntityResponseType = HttpResponse<IDeliveryNoteEntry>;
type EntityArrayResponseType = HttpResponse<IDeliveryNoteEntry[]>;

@Injectable({ providedIn: 'root' })
export class DeliveryNoteEntryService {
    private resourceUrl = SERVER_API_URL + 'deliverynoteservice/api/delivery-note-entries';

    constructor(private http: HttpClient) {}

    create(deliveryNoteEntry: IDeliveryNoteEntry): Observable<EntityResponseType> {
        return this.http.post<IDeliveryNoteEntry>(this.resourceUrl, deliveryNoteEntry, { observe: 'response' });
    }

    update(deliveryNoteEntry: IDeliveryNoteEntry): Observable<EntityResponseType> {
        return this.http.put<IDeliveryNoteEntry>(this.resourceUrl, deliveryNoteEntry, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IDeliveryNoteEntry>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IDeliveryNoteEntry[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
