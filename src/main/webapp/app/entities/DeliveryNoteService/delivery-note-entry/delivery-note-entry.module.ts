import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import {
    DeliveryNoteEntryComponent,
    DeliveryNoteEntryDetailComponent,
    DeliveryNoteEntryUpdateComponent,
    DeliveryNoteEntryDeletePopupComponent,
    DeliveryNoteEntryDeleteDialogComponent,
    deliveryNoteEntryRoute,
    deliveryNoteEntryPopupRoute
} from './';

const ENTITY_STATES = [...deliveryNoteEntryRoute, ...deliveryNoteEntryPopupRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        DeliveryNoteEntryComponent,
        DeliveryNoteEntryDetailComponent,
        DeliveryNoteEntryUpdateComponent,
        DeliveryNoteEntryDeleteDialogComponent,
        DeliveryNoteEntryDeletePopupComponent
    ],
    entryComponents: [
        DeliveryNoteEntryComponent,
        DeliveryNoteEntryUpdateComponent,
        DeliveryNoteEntryDeleteDialogComponent,
        DeliveryNoteEntryDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayDeliveryNoteEntryModule {}
