import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDeliveryNoteEntry } from 'app/shared/model/DeliveryNoteService/delivery-note-entry.model';
import { DeliveryNoteEntryService } from './delivery-note-entry.service';

@Component({
    selector: 'jhi-delivery-note-entry-delete-dialog',
    templateUrl: './delivery-note-entry-delete-dialog.component.html'
})
export class DeliveryNoteEntryDeleteDialogComponent {
    deliveryNoteEntry: IDeliveryNoteEntry;

    constructor(
        private deliveryNoteEntryService: DeliveryNoteEntryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.deliveryNoteEntryService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'deliveryNoteEntryListModification',
                content: 'Deleted an deliveryNoteEntry'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-delivery-note-entry-delete-popup',
    template: ''
})
export class DeliveryNoteEntryDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ deliveryNoteEntry }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(DeliveryNoteEntryDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.deliveryNoteEntry = deliveryNoteEntry;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
