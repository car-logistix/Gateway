import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { GatewayInvoiceModule as InvoiceServiceInvoiceModule } from './InvoiceService/invoice/invoice.module';
import { GatewayDeliveryNoteModule as DeliveryNoteServiceDeliveryNoteModule } from './DeliveryNoteService/delivery-note/delivery-note.module';
import { GatewayInvoiceProcessModule as InvoiceCheckingServiceInvoiceProcessModule } from './InvoiceCheckingService/invoice-process/invoice-process.module';
import { GatewayDeliveryNoteEntryModule as DeliveryNoteServiceDeliveryNoteEntryModule } from './DeliveryNoteService/delivery-note-entry/delivery-note-entry.module';
import { GatewayInvoiceCommentModule as InvoiceServiceInvoiceCommentModule } from './InvoiceService/invoice-comment/invoice-comment.module';
// prettier-ignore
import {
    GatewayInvoiceProcessCommentModule as InvoiceCheckingServiceInvoiceProcessCommentModule
} from './InvoiceCheckingService/invoice-process-comment/invoice-process-comment.module';
import { GatewayInvoiceEntryModule as InvoiceServiceInvoiceEntryModule } from './InvoiceService/invoice-entry/invoice-entry.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        InvoiceServiceInvoiceModule,
        DeliveryNoteServiceDeliveryNoteModule,
        InvoiceCheckingServiceInvoiceProcessModule,
        DeliveryNoteServiceDeliveryNoteEntryModule,
        InvoiceServiceInvoiceCommentModule,
        InvoiceCheckingServiceInvoiceProcessCommentModule,
        InvoiceServiceInvoiceEntryModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayEntityModule {}
