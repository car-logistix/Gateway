import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IInvoiceProcess } from 'app/shared/model/InvoiceCheckingService/invoice-process.model';

type EntityResponseType = HttpResponse<IInvoiceProcess>;
type EntityArrayResponseType = HttpResponse<IInvoiceProcess[]>;

@Injectable({ providedIn: 'root' })
export class InvoiceProcessService {
    private resourceUrl = SERVER_API_URL + 'invoicecheckingservice/api/invoice-processes';

    constructor(private http: HttpClient) {}

    create(invoiceProcess: IInvoiceProcess): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(invoiceProcess);
        return this.http
            .post<IInvoiceProcess>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(invoiceProcess: IInvoiceProcess): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(invoiceProcess);
        return this.http
            .put<IInvoiceProcess>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IInvoiceProcess>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IInvoiceProcess[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    escalate(id: number): Observable<HttpResponse<any>> {
        return this.http.post<any>(`${this.resourceUrl}/${id}/escalate`, { observe: 'response' });
    }
    finish(id: number): Observable<HttpResponse<any>> {
        return this.http.post<any>(`${this.resourceUrl}/${id}/finish`, { observe: 'response' });
    }

    private convertDateFromClient(invoiceProcess: IInvoiceProcess): IInvoiceProcess {
        const copy: IInvoiceProcess = Object.assign({}, invoiceProcess, {
            postedAt: invoiceProcess.postedAt != null && invoiceProcess.postedAt.isValid() ? invoiceProcess.postedAt.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.postedAt = res.body.postedAt != null ? moment(res.body.postedAt) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((invoiceProcess: IInvoiceProcess) => {
            invoiceProcess.postedAt = invoiceProcess.postedAt != null ? moment(invoiceProcess.postedAt) : null;
        });
        return res;
    }
}
