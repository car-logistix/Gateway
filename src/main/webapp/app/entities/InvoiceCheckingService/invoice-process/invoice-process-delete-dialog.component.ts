import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IInvoiceProcess } from 'app/shared/model/InvoiceCheckingService/invoice-process.model';
import { InvoiceProcessService } from './invoice-process.service';

@Component({
    selector: 'jhi-invoice-process-delete-dialog',
    templateUrl: './invoice-process-delete-dialog.component.html'
})
export class InvoiceProcessDeleteDialogComponent {
    invoiceProcess: IInvoiceProcess;

    constructor(
        private invoiceProcessService: InvoiceProcessService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.invoiceProcessService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'invoiceProcessListModification',
                content: 'Deleted an invoiceProcess'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-invoice-process-delete-popup',
    template: ''
})
export class InvoiceProcessDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ invoiceProcess }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(InvoiceProcessDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.invoiceProcess = invoiceProcess;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
