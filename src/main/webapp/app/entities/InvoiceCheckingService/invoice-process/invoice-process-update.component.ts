import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IInvoiceProcess } from 'app/shared/model/InvoiceCheckingService/invoice-process.model';
import { InvoiceProcessService } from './invoice-process.service';
import { InvoiceService } from 'app/entities/InvoiceService/invoice';
import { IInvoice } from 'app/shared/model/InvoiceService/invoice.model';
import { IDeliveryNote } from 'app/shared/model/DeliveryNoteService/delivery-note.model';
import { DeliveryNoteService } from 'app/entities/DeliveryNoteService/delivery-note';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { IInvoiceProcessComment } from 'app/shared/model/InvoiceCheckingService/invoice-process-comment.model';
import { Principal } from 'app/core';

@Component({
    selector: 'jhi-invoice-process-update',
    templateUrl: './invoice-process-update.component.html'
})
export class InvoiceProcessUpdateComponent implements OnInit {
    private _invoiceProcess: IInvoiceProcess;
    isSaving: boolean;
    postedAt: string;
    invoice: IInvoice;
    deliveryNote: IDeliveryNote;
    newComment: IInvoiceProcessComment = {};
    account: any;
    eventSubscriber: Subscription;

    constructor(
        private invoiceProcessService: InvoiceProcessService,
        private invoiceService: InvoiceService,
        private deliveryNoteService: DeliveryNoteService,
        private activatedRoute: ActivatedRoute,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private eventManager: JhiEventManager
    ) {}

    loadAll() {
        console.log('I saw the update and running loadAll');
        this.activatedRoute.data.subscribe(({ invoiceProcess }) => {
            this.invoiceProcess = invoiceProcess;
            this.invoiceService.find(this.invoiceProcess.invoiceId).subscribe(
                (res: HttpResponse<IInvoice>) => {
                    this.invoice = res.body;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
            if (this.invoiceProcess.deliveryNoteId) {
                this.deliveryNoteService.find(this.invoiceProcess.deliveryNoteId).subscribe(
                    (res: HttpResponse<IInvoice>) => {
                        this.deliveryNote = res.body;
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            }
        });
    }
    ngOnInit() {
        console.log('init');
        this.isSaving = false;
        this.loadAll();
        this.principal.identity().then(account => {
            this.account = account;
            this.registerChangeInInvoiceProcesses();
        });
    }

    previousState() {
        window.history.back();
    }

    addComment() {
        this.newComment.user = this.account.login;
        this.invoiceProcess.comments = this.invoiceProcess.comments || [];
        this.invoiceProcess.comments.push(this.newComment);
        this.newComment = {};
    }

    escalate() {
        this.invoiceProcessService.escalate(this.invoiceProcess.id).subscribe(
            (res: HttpResponse<void>) => {
                this.previousState();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    finish() {
        this.invoiceProcessService.finish(this.invoiceProcess.id).subscribe(
            (res: HttpResponse<void>) => {
                this.previousState();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    save() {
        this.isSaving = true;
        this.invoiceProcess.postedAt = moment(this.postedAt, DATE_TIME_FORMAT);
        if (this.invoiceProcess.id !== undefined) {
            this.subscribeToSaveResponse(this.invoiceProcessService.update(this.invoiceProcess));
        } else {
            this.subscribeToSaveResponse(this.invoiceProcessService.create(this.invoiceProcess));
        }
    }

    registerChangeInInvoiceProcesses() {
        this.eventSubscriber = this.eventManager.subscribe('invoiceProcessUpdateModification', response => {
            this.invoiceProcessService.find(this.invoiceProcess.id).subscribe((res: HttpResponse<IInvoiceProcess>) => {
                this.invoiceProcess = res.body;
                this.invoiceService.find(this.invoiceProcess.invoiceId).subscribe(
                    (res: HttpResponse<IInvoice>) => {
                        this.invoice = res.body;
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
                if (this.invoiceProcess.deliveryNoteId) {
                    this.deliveryNoteService.find(this.invoiceProcess.deliveryNoteId).subscribe(
                        (res: HttpResponse<IInvoice>) => {
                            this.deliveryNote = res.body;
                        },
                        (res: HttpErrorResponse) => this.onError(res.message)
                    );
                }
            });
        });
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IInvoiceProcess>>) {
        result.subscribe((res: HttpResponse<IInvoiceProcess>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    get invoiceProcess() {
        return this._invoiceProcess;
    }

    set invoiceProcess(invoiceProcess: IInvoiceProcess) {
        this._invoiceProcess = invoiceProcess;
        this.postedAt = moment(invoiceProcess.postedAt).format(DATE_TIME_FORMAT);
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
