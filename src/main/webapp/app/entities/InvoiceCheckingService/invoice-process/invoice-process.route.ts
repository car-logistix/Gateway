import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { InvoiceProcess } from 'app/shared/model/InvoiceCheckingService/invoice-process.model';
import { InvoiceProcessService } from './invoice-process.service';
import { InvoiceProcessComponent } from './invoice-process.component';
import { InvoiceProcessDetailComponent } from './invoice-process-detail.component';
import { InvoiceProcessUpdateComponent } from './invoice-process-update.component';
import { InvoiceProcessDeletePopupComponent } from './invoice-process-delete-dialog.component';
import { IInvoiceProcess } from 'app/shared/model/InvoiceCheckingService/invoice-process.model';
import { DeliveryNoteChooserPopupComponent } from 'app/entities/InvoiceCheckingService/invoice-process/delivery-note-chooser-dialog.component';

@Injectable({ providedIn: 'root' })
export class InvoiceProcessResolve implements Resolve<IInvoiceProcess> {
    constructor(private service: InvoiceProcessService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((invoiceProcess: HttpResponse<InvoiceProcess>) => invoiceProcess.body));
        }
        return of(new InvoiceProcess());
    }
}

export const invoiceProcessRoute: Routes = [
    {
        path: 'invoice-process',
        component: InvoiceProcessComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceProcesses'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice-process/:id/view',
        component: InvoiceProcessDetailComponent,
        resolve: {
            invoiceProcess: InvoiceProcessResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceProcesses'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice-process/new',
        component: InvoiceProcessUpdateComponent,
        resolve: {
            invoiceProcess: InvoiceProcessResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceProcesses'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice-process/:id/edit',
        component: InvoiceProcessUpdateComponent,
        resolve: {
            invoiceProcess: InvoiceProcessResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceProcesses'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const invoiceProcessPopupRoute: Routes = [
    {
        path: 'invoice-process/:id/delete',
        component: InvoiceProcessDeletePopupComponent,
        resolve: {
            invoiceProcess: InvoiceProcessResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceProcesses'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'invoice-process/:id/choose-delivery-note',
        component: DeliveryNoteChooserPopupComponent,
        resolve: {
            invoiceProcess: InvoiceProcessResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceProcesses'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
