import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IInvoiceProcess } from 'app/shared/model/InvoiceCheckingService/invoice-process.model';
import { Principal } from 'app/core';
import { InvoiceProcessService } from './invoice-process.service';

@Component({
    selector: 'jhi-invoice-process',
    templateUrl: './invoice-process.component.html'
})
export class InvoiceProcessComponent implements OnInit, OnDestroy {
    invoiceProcesses: IInvoiceProcess[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private invoiceProcessService: InvoiceProcessService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.invoiceProcessService.query().subscribe(
            (res: HttpResponse<IInvoiceProcess[]>) => {
                this.invoiceProcesses = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInInvoiceProcesses();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IInvoiceProcess) {
        return item.id;
    }

    registerChangeInInvoiceProcesses() {
        this.eventSubscriber = this.eventManager.subscribe('invoiceProcessListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
