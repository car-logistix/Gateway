import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import {
    InvoiceProcessComponent,
    InvoiceProcessDetailComponent,
    InvoiceProcessUpdateComponent,
    InvoiceProcessDeletePopupComponent,
    InvoiceProcessDeleteDialogComponent,
    invoiceProcessRoute,
    invoiceProcessPopupRoute,
    DeliveryNoteChooserPopupComponent,
    DeliveryNoteChooserDialogComponent
} from './';

const ENTITY_STATES = [...invoiceProcessRoute, ...invoiceProcessPopupRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        InvoiceProcessComponent,
        InvoiceProcessDetailComponent,
        InvoiceProcessUpdateComponent,
        InvoiceProcessDeleteDialogComponent,
        InvoiceProcessDeletePopupComponent,
        DeliveryNoteChooserPopupComponent,
        DeliveryNoteChooserDialogComponent
    ],
    entryComponents: [
        InvoiceProcessComponent,
        InvoiceProcessUpdateComponent,
        InvoiceProcessDeleteDialogComponent,
        InvoiceProcessDeletePopupComponent,
        DeliveryNoteChooserPopupComponent,
        DeliveryNoteChooserDialogComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayInvoiceProcessModule {}
