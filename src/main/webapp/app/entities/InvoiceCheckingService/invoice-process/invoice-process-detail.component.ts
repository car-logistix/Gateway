import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IInvoiceProcess } from 'app/shared/model/InvoiceCheckingService/invoice-process.model';

@Component({
    selector: 'jhi-invoice-process-detail',
    templateUrl: './invoice-process-detail.component.html'
})
export class InvoiceProcessDetailComponent implements OnInit {
    invoiceProcess: IInvoiceProcess;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ invoiceProcess }) => {
            this.invoiceProcess = invoiceProcess;
        });
    }

    previousState() {
        window.history.back();
    }
}
