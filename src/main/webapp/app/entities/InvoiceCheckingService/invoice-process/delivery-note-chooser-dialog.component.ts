import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { IInvoiceProcess } from 'app/shared/model/InvoiceCheckingService/invoice-process.model';
import { InvoiceProcessService } from './invoice-process.service';
import { DeliveryNoteService } from 'app/entities/DeliveryNoteService/delivery-note';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { IDeliveryNote } from 'app/shared/model/DeliveryNoteService/delivery-note.model';

@Component({
    selector: 'jhi-delivery-note-chooser-dialog',
    templateUrl: './delivery-note-chooser-dialog.component.html'
})
export class DeliveryNoteChooserDialogComponent implements OnInit {
    invoiceProcess: IInvoiceProcess;
    deliveryNotes: IDeliveryNote[];
    selectedId: number;

    constructor(
        private invoiceProcessService: InvoiceProcessService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager,
        private deliveryNoteService: DeliveryNoteService,
        private jhiAlertService: JhiAlertService
    ) {}

    ngOnInit(): void {
        this.deliveryNoteService.query().subscribe(
            (res: HttpResponse<IDeliveryNote[]>) => {
                this.deliveryNotes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    selectDeliveryNote(id: number) {
        this.selectedId = id;
    }

    confirmSelection() {
        this.invoiceProcess.deliveryNoteId = this.selectedId;
        this.invoiceProcessService.update(this.invoiceProcess).subscribe(response => {
            this.eventManager.broadcast({
                name: 'invoiceProcessUpdateModification',
                content: 'Choose an Delivery Note'
            });
            this.activeModal.dismiss(true);
        });
    }

    private onError(message: string) {
        this.jhiAlertService.error(message, null, null);
    }
}

@Component({
    selector: 'jhi-delivery-note-chooser-popup',
    template: ''
})
export class DeliveryNoteChooserPopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ invoiceProcess }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(DeliveryNoteChooserDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.invoiceProcess = invoiceProcess;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], {
                            replaceUrl: true,
                            queryParamsHandling: 'merge'
                        });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], {
                            replaceUrl: true,
                            queryParamsHandling: 'merge'
                        });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
