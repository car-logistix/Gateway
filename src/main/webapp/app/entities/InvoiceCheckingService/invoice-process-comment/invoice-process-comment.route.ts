import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { InvoiceProcessComment } from 'app/shared/model/InvoiceCheckingService/invoice-process-comment.model';
import { InvoiceProcessCommentService } from './invoice-process-comment.service';
import { InvoiceProcessCommentComponent } from './invoice-process-comment.component';
import { InvoiceProcessCommentDetailComponent } from './invoice-process-comment-detail.component';
import { InvoiceProcessCommentUpdateComponent } from './invoice-process-comment-update.component';
import { InvoiceProcessCommentDeletePopupComponent } from './invoice-process-comment-delete-dialog.component';
import { IInvoiceProcessComment } from 'app/shared/model/InvoiceCheckingService/invoice-process-comment.model';

@Injectable({ providedIn: 'root' })
export class InvoiceProcessCommentResolve implements Resolve<IInvoiceProcessComment> {
    constructor(private service: InvoiceProcessCommentService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service
                .find(id)
                .pipe(map((invoiceProcessComment: HttpResponse<InvoiceProcessComment>) => invoiceProcessComment.body));
        }
        return of(new InvoiceProcessComment());
    }
}

export const invoiceProcessCommentRoute: Routes = [
    {
        path: 'invoice-process-comment',
        component: InvoiceProcessCommentComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceProcessComments'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice-process-comment/:id/view',
        component: InvoiceProcessCommentDetailComponent,
        resolve: {
            invoiceProcessComment: InvoiceProcessCommentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceProcessComments'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice-process-comment/new',
        component: InvoiceProcessCommentUpdateComponent,
        resolve: {
            invoiceProcessComment: InvoiceProcessCommentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceProcessComments'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'invoice-process-comment/:id/edit',
        component: InvoiceProcessCommentUpdateComponent,
        resolve: {
            invoiceProcessComment: InvoiceProcessCommentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceProcessComments'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const invoiceProcessCommentPopupRoute: Routes = [
    {
        path: 'invoice-process-comment/:id/delete',
        component: InvoiceProcessCommentDeletePopupComponent,
        resolve: {
            invoiceProcessComment: InvoiceProcessCommentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'InvoiceProcessComments'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
