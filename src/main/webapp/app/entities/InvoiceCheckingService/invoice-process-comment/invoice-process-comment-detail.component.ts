import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IInvoiceProcessComment } from 'app/shared/model/InvoiceCheckingService/invoice-process-comment.model';

@Component({
    selector: 'jhi-invoice-process-comment-detail',
    templateUrl: './invoice-process-comment-detail.component.html'
})
export class InvoiceProcessCommentDetailComponent implements OnInit {
    invoiceProcessComment: IInvoiceProcessComment;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ invoiceProcessComment }) => {
            this.invoiceProcessComment = invoiceProcessComment;
        });
    }

    previousState() {
        window.history.back();
    }
}
