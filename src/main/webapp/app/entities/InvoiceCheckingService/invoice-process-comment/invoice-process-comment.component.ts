import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IInvoiceProcessComment } from 'app/shared/model/InvoiceCheckingService/invoice-process-comment.model';
import { Principal } from 'app/core';
import { InvoiceProcessCommentService } from './invoice-process-comment.service';

@Component({
    selector: 'jhi-invoice-process-comment',
    templateUrl: './invoice-process-comment.component.html'
})
export class InvoiceProcessCommentComponent implements OnInit, OnDestroy {
    invoiceProcessComments: IInvoiceProcessComment[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private invoiceProcessCommentService: InvoiceProcessCommentService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.invoiceProcessCommentService.query().subscribe(
            (res: HttpResponse<IInvoiceProcessComment[]>) => {
                this.invoiceProcessComments = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInInvoiceProcessComments();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IInvoiceProcessComment) {
        return item.id;
    }

    registerChangeInInvoiceProcessComments() {
        this.eventSubscriber = this.eventManager.subscribe('invoiceProcessCommentListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
