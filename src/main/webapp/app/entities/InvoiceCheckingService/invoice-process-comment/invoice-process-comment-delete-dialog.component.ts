import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IInvoiceProcessComment } from 'app/shared/model/InvoiceCheckingService/invoice-process-comment.model';
import { InvoiceProcessCommentService } from './invoice-process-comment.service';

@Component({
    selector: 'jhi-invoice-process-comment-delete-dialog',
    templateUrl: './invoice-process-comment-delete-dialog.component.html'
})
export class InvoiceProcessCommentDeleteDialogComponent {
    invoiceProcessComment: IInvoiceProcessComment;

    constructor(
        private invoiceProcessCommentService: InvoiceProcessCommentService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.invoiceProcessCommentService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'invoiceProcessCommentListModification',
                content: 'Deleted an invoiceProcessComment'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-invoice-process-comment-delete-popup',
    template: ''
})
export class InvoiceProcessCommentDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ invoiceProcessComment }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(InvoiceProcessCommentDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.invoiceProcessComment = invoiceProcessComment;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
