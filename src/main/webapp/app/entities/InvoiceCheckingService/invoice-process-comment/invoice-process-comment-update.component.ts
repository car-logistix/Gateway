import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IInvoiceProcessComment } from 'app/shared/model/InvoiceCheckingService/invoice-process-comment.model';
import { InvoiceProcessCommentService } from './invoice-process-comment.service';
import { IInvoiceProcess } from 'app/shared/model/InvoiceCheckingService/invoice-process.model';
import { InvoiceProcessService } from 'app/entities/InvoiceCheckingService/invoice-process';

@Component({
    selector: 'jhi-invoice-process-comment-update',
    templateUrl: './invoice-process-comment-update.component.html'
})
export class InvoiceProcessCommentUpdateComponent implements OnInit {
    private _invoiceProcessComment: IInvoiceProcessComment;
    isSaving: boolean;

    invoiceprocesses: IInvoiceProcess[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private invoiceProcessCommentService: InvoiceProcessCommentService,
        private invoiceProcessService: InvoiceProcessService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ invoiceProcessComment }) => {
            this.invoiceProcessComment = invoiceProcessComment;
        });
        this.invoiceProcessService.query().subscribe(
            (res: HttpResponse<IInvoiceProcess[]>) => {
                this.invoiceprocesses = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.invoiceProcessComment.id !== undefined) {
            this.subscribeToSaveResponse(this.invoiceProcessCommentService.update(this.invoiceProcessComment));
        } else {
            this.subscribeToSaveResponse(this.invoiceProcessCommentService.create(this.invoiceProcessComment));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IInvoiceProcessComment>>) {
        result.subscribe(
            (res: HttpResponse<IInvoiceProcessComment>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackInvoiceProcessById(index: number, item: IInvoiceProcess) {
        return item.id;
    }
    get invoiceProcessComment() {
        return this._invoiceProcessComment;
    }

    set invoiceProcessComment(invoiceProcessComment: IInvoiceProcessComment) {
        this._invoiceProcessComment = invoiceProcessComment;
    }
}
