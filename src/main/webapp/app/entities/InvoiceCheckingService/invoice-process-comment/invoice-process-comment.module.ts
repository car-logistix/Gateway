import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import {
    InvoiceProcessCommentComponent,
    InvoiceProcessCommentDetailComponent,
    InvoiceProcessCommentUpdateComponent,
    InvoiceProcessCommentDeletePopupComponent,
    InvoiceProcessCommentDeleteDialogComponent,
    invoiceProcessCommentRoute,
    invoiceProcessCommentPopupRoute
} from './';

const ENTITY_STATES = [...invoiceProcessCommentRoute, ...invoiceProcessCommentPopupRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        InvoiceProcessCommentComponent,
        InvoiceProcessCommentDetailComponent,
        InvoiceProcessCommentUpdateComponent,
        InvoiceProcessCommentDeleteDialogComponent,
        InvoiceProcessCommentDeletePopupComponent
    ],
    entryComponents: [
        InvoiceProcessCommentComponent,
        InvoiceProcessCommentUpdateComponent,
        InvoiceProcessCommentDeleteDialogComponent,
        InvoiceProcessCommentDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayInvoiceProcessCommentModule {}
